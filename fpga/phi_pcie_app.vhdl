library ieee;

use ieee.std_logic_1164.ALL;

entity phi_pcie_app is
	port(
		-- asynchronous reset
		reset_n : in std_logic;

		-- PCIe clock
		pcie_clk : in std_logic;

		-- receive side, internal PCIe interface (Avalon-ST)
		pcie_rx_ready : out std_logic;
		pcie_rx_valid : in std_logic;
		pcie_rx_data : in std_logic_vector(63 downto 0);
		pcie_rx_sop : in std_logic;
		pcie_rx_eop : in std_logic;
		pcie_rx_err : in std_logic;

		-- receive side, extra signals
		pcie_rx_mask : out std_logic;
		pcie_rx_bardec : in std_logic_vector(7 downto 0);

		-- send side, internal PCIe interface (Avalon-ST)
		pcie_tx_ready : in std_logic;
		pcie_tx_valid : out std_logic;
		pcie_tx_data : out std_logic_vector(63 downto 0);
		pcie_tx_sop : out std_logic;
		pcie_tx_eop : out std_logic;
		pcie_tx_err : out std_logic;

		-- arbiter interface
		pcie_tx_req : out std_logic;
		pcie_tx_start : in std_logic;

		-- PCIe power management
		pcie_cpl_pending : out std_logic;

		-- completer ID (from configuration space)
		pcie_cmpl_id : in std_logic_vector(15 downto 0)
	);
end entity;

architecture rtl of phi_pcie_app is
	-- PCIe receive for virtualmap (Avalon-ST)
	signal virtualmap_rx_ready : std_logic;
	signal virtualmap_rx_valid : std_logic;
	signal virtualmap_rx_data : std_logic_vector(63 downto 0);
	signal virtualmap_rx_sop : std_logic;
	signal virtualmap_rx_eop : std_logic;
	signal virtualmap_rx_err : std_logic;

	-- PCIe receive for virtualmap (extra signals)
	signal virtualmap_rx_mask : std_logic;
	signal virtualmap_rx_bardec : std_logic_vector(7 downto 0);

	-- PCIe transmit for virtualmap (Avalon-ST)
	signal virtualmap_tx_ready : std_logic;
	signal virtualmap_tx_valid : std_logic;
	signal virtualmap_tx_data : std_logic_vector(63 downto 0);
	signal virtualmap_tx_sop : std_logic;
	signal virtualmap_tx_eop : std_logic;
	signal virtualmap_tx_err : std_logic;

	-- PCIe transmit for virtualmap (arbiter)
	signal virtualmap_tx_req : std_logic;
	signal virtualmap_tx_start : std_logic;

	-- PCIe power management for virtualmap
	signal virtualmap_cpl_pending : std_logic;

	-- PCIe receive for dma_write (Avalon-ST)
	signal dma_write_rx_ready : std_logic;
	signal dma_write_rx_valid : std_logic;
	signal dma_write_rx_data : std_logic_vector(63 downto 0);
	signal dma_write_rx_sop : std_logic;
	signal dma_write_rx_eop : std_logic;
	signal dma_write_rx_err : std_logic;

	-- PCIe receive for dma_write (extra signals)
	signal dma_write_rx_mask : std_logic;
	signal dma_write_rx_bardec : std_logic_vector(7 downto 0);

	-- PCIe transmit for dma_write (Avalon-ST)
	signal dma_write_tx_ready : std_logic;
	signal dma_write_tx_valid : std_logic;
	signal dma_write_tx_data : std_logic_vector(63 downto 0);
	signal dma_write_tx_sop : std_logic;
	signal dma_write_tx_eop : std_logic;
	signal dma_write_tx_err : std_logic;

	-- PCIe transmit for dma_write (arbiter)
	signal dma_write_tx_req : std_logic;
	signal dma_write_tx_start : std_logic;

	-- PCIe power management for dma_write
	signal dma_write_cpl_pending : std_logic;
begin
	pcie_rx_ready <= virtualmap_rx_ready and dma_write_rx_ready;
	pcie_rx_mask <= virtualmap_rx_mask or dma_write_rx_mask;

	virtualmap_rx_valid <= pcie_rx_valid;
	virtualmap_rx_data <= pcie_rx_data;
	virtualmap_rx_sop <= pcie_rx_sop;
	virtualmap_rx_eop <= pcie_rx_eop;
	virtualmap_rx_err <= pcie_rx_err;

	virtualmap_rx_bardec <= pcie_rx_bardec;

	dma_write_rx_valid <= pcie_rx_valid;
	dma_write_rx_data <= pcie_rx_data;
	dma_write_rx_sop <= pcie_rx_sop;
	dma_write_rx_eop <= pcie_rx_eop;
	dma_write_rx_err <= pcie_rx_err;

	dma_write_rx_bardec <= pcie_rx_bardec;

	arbiter_inst : entity work.pcie_arbiter
		generic map(
			num_agents => 2
		)
		port map(
			clk => pcie_clk,
			reset_n => reset_n,

			merged_tx_req => pcie_tx_req,
			merged_tx_start => pcie_tx_start,

			merged_tx_ready => pcie_tx_ready,
			merged_tx_valid => pcie_tx_valid,
			merged_tx_data => pcie_tx_data,
			merged_tx_sop => pcie_tx_sop,
			merged_tx_eop => pcie_tx_eop,
			merged_tx_err => pcie_tx_err,

			merged_cpl_pending => pcie_cpl_pending,

			arb_tx_req(1) => virtualmap_tx_req,
			arb_tx_start(1) => virtualmap_tx_start,

			arb_tx_ready(1) => virtualmap_tx_ready,
			arb_tx_valid(1) => virtualmap_tx_valid,
			arb_tx_data(1) => virtualmap_tx_data,
			arb_tx_sop(1) => virtualmap_tx_sop,
			arb_tx_eop(1) => virtualmap_tx_eop,
			arb_tx_err(1) => virtualmap_tx_err,

			arb_cpl_pending(1) => virtualmap_cpl_pending,

			arb_tx_req(2) => dma_write_tx_req,
			arb_tx_start(2) => dma_write_tx_start,

			arb_tx_ready(2) => dma_write_tx_ready,
			arb_tx_valid(2) => dma_write_tx_valid,
			arb_tx_data(2) => dma_write_tx_data,
			arb_tx_sop(2) => dma_write_tx_sop,
			arb_tx_eop(2) => dma_write_tx_eop,
			arb_tx_err(2) => dma_write_tx_err,

			arb_cpl_pending(2) => dma_write_cpl_pending
		);

	virtualmap_inst : entity work.virtualmap_pcie
		port map(
			-- asynchronous reset
			reset_n => reset_n,

			-- data interface (read-only)
			address => (others => '0'),
			rden => '0',
			q => open,

			-- PCIe clock
			pcie_clk => pcie_clk,

			-- receive side, internal PCIe interface (Avalon-ST)
			pcie_rx_ready => virtualmap_rx_ready,
			pcie_rx_valid => virtualmap_rx_valid,
			pcie_rx_data => virtualmap_rx_data,
			pcie_rx_sop => virtualmap_rx_sop,
			pcie_rx_eop => virtualmap_rx_eop,
			pcie_rx_err => virtualmap_rx_err,

			-- receive side, extra signals
			pcie_rx_mask => virtualmap_rx_mask,
			pcie_rx_bardec => virtualmap_rx_bardec,

			-- send side, internal PCIe interface (Avalon-ST)
			pcie_tx_ready => virtualmap_tx_ready,
			pcie_tx_valid => virtualmap_tx_valid,
			pcie_tx_data => virtualmap_tx_data,
			pcie_tx_sop => virtualmap_tx_sop,
			pcie_tx_eop => virtualmap_tx_eop,
			pcie_tx_err => virtualmap_tx_err,

			-- arbiter interface
			pcie_tx_req => virtualmap_tx_req,
			pcie_tx_start => virtualmap_tx_start,

			-- power management
			pcie_cpl_pending => virtualmap_cpl_pending,

			-- completer ID (from configuration space)
			pcie_cmpl_id => pcie_cmpl_id
		);

	dma_write_inst : entity work.dma_write
		port map(
			-- asynchronous reset
			reset_n => reset_n,

			-- PCIe clock
			pcie_clk => pcie_clk,

			-- control interface
			address => (others => '0'),
			size => (others => '0'),
			start => '0',
			finished => open,

			-- data interface
			data_ready => open,
			data_valid => '0',
			data_data => (others => '0'),

			-- receive side, internal PCIe interface (Avalon-ST)
			pcie_rx_ready => dma_write_rx_ready,
			pcie_rx_valid => dma_write_rx_valid,
			pcie_rx_data => dma_write_rx_data,
			pcie_rx_sop => dma_write_rx_sop,
			pcie_rx_eop => dma_write_rx_eop,
			pcie_rx_err => dma_write_rx_err,

			-- receive side, extra signals
			pcie_rx_mask => dma_write_rx_mask,
			pcie_rx_bardec => dma_write_rx_bardec,

			-- send side, internal PCIe interface (Avalon-ST)
			pcie_tx_ready => dma_write_tx_ready,
			pcie_tx_valid => dma_write_tx_valid,
			pcie_tx_data => dma_write_tx_data,
			pcie_tx_sop => dma_write_tx_sop,
			pcie_tx_eop => dma_write_tx_eop,
			pcie_tx_err => dma_write_tx_err,

			-- arbiter interface
			pcie_tx_req => dma_write_tx_req,
			pcie_tx_start => dma_write_tx_start,

			-- power management
			pcie_cpl_pending => dma_write_cpl_pending,

			-- completer ID (from configuration space)
			pcie_cmpl_id => pcie_cmpl_id
		);
end architecture;
