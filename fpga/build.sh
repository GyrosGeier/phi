#! /bin/sh

set -e

if ! which quartus_sh >/dev/null; then
	PATH=/opt/altera/16.1/quartus/bin:$PATH
fi

enable_tbbmalloc_workaround()
{
	LD_PRELOAD='/opt/${LIB}/hack.so'
	export LD_PRELOAD
}

disable_tbbmalloc_workaround()
{
	unset LD_PRELOAD
}

if [ "$1" = "-r" ]; then
	if [ -z "$DISPLAY" ]; then
		if [ -z "`which xvfb-run`" ]; then
			echo >&2 "MegaWizard requires an X server."
			echo >&2
			echo >&2 "Either run from a graphical session or install xvfb"
			exit 1
		fi
		# reexec self under xvfb
		exec xvfb-run $0 "$*"
	fi
	qmegawiz -silent gpio.vhd
	qmegawiz -silent pcie.vhd
	qmegawiz -silent pcie_serdes.vhd
	qmegawiz -silent pcie_reconfig.vhd
	qmegawiz -silent pll.vhd
	qmegawiz -silent virtualmap.vhd

	# patch frequency of fixedclk_serdes
	sed -i -e '/fixedclk_serdes/s/100/125/' pcie.sdc
	# patch multicycle constraints for tl_cfg sampling
	sed -i -e '/set_multicycle_path/s/tl_cfg_[a-z_]*/&_hip/' pcie.sdc
	# copy tl_cfg sampling code
	cp pcie_examples/chaining_dma/altpcierd_tl_cfg_sample.vhd .
	# copy reset controller
	cp pcie_examples/chaining_dma/pcie_rs_hip.vhd .
fi

enable_tbbmalloc_workaround

quartus_sh --flow compile phi.qpf
quartus_cpf -c -n p -q 10MHz -g 2.5V output_files/phi.sof output_files/phi.svf

disable_tbbmalloc_workaround
