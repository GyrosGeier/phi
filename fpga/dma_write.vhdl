library ieee;

use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;

entity dma_write is
	port(
		reset_n : in std_logic;

		pcie_clk : in std_logic;

		address : in std_logic_vector(63 downto 0);
		size : in std_logic_vector(63 downto 0);
		start : in std_logic;
		finished : out std_logic;

		data_ready : out std_logic;
		data_valid : in std_logic;
		data_data : in std_logic_vector(63 downto 0);
		
		pcie_rx_ready : out std_logic;
		pcie_rx_valid : in std_logic;
		pcie_rx_data : in std_logic_vector(63 downto 0);
		pcie_rx_sop : in std_logic;
		pcie_rx_eop : in std_logic;
		pcie_rx_err : in std_logic;

		pcie_rx_mask : out std_logic;
		-- valid during second cycle in 64 bit interface
		pcie_rx_bardec : in std_logic_vector(7 downto 0);

		pcie_tx_ready : in std_logic;
		pcie_tx_valid : out std_logic;
		pcie_tx_data : out std_logic_vector(63 downto 0);
		pcie_tx_sop : out std_logic;
		pcie_tx_eop : out std_logic;
		pcie_tx_err : out std_logic;

		pcie_tx_req : out std_logic;
		pcie_tx_start : in std_logic;

		pcie_cpl_pending : out std_logic;

		pcie_cmpl_id : in std_logic_vector(15 downto 0)
	);
end entity;

architecture syn of dma_write is
	type state is (idle, waiting, header, data);
	
	signal tx_state : state;

	signal remaining : unsigned(63 downto 0);
	
	constant nullptr : std_logic_vector(63 downto 0) := (others => '0');
begin
	pcie_rx_ready <= '1';
	pcie_rx_mask <= '0';
	pcie_cpl_pending <= '0';
	pcie_tx_err <= '0';
	
	pcie_tx_req <= '1' when (tx_state = waiting)
			else '0';
	
	finished <= '1' when (tx_state = idle) else '0';

	data_ready <= '1' when (tx_state = data) else '0';
	
	process(reset_n, pcie_clk)
	begin
		if(reset_n = '0') then
			tx_state <= idle;

			pcie_tx_valid <= '0';
			pcie_tx_sop <= 'U';
			pcie_tx_eop <= 'U';
			pcie_tx_data <= (others => 'U');
		elsif(rising_edge(pcie_clk)) then
			pcie_tx_valid <= '0';
			pcie_tx_sop <= 'U';
			pcie_tx_eop <= 'U';
			pcie_tx_data <= (others => 'U');							
			if(pcie_tx_ready = '1') then
				case tx_state is
					when idle =>
						if(start = '1') then
							tx_state <= waiting;
							remaining <= unsigned(size);
						end if;
					when waiting =>
						if (pcie_tx_start = '1') then
							pcie_tx_valid <= '1';
							pcie_tx_sop <= '1';
							pcie_tx_eop <= '0';
							pcie_tx_data <=
									pcie_cmpl_id &		-- requester id
									"00000000" &		-- tag
									"11111111" &		-- byte enable
									"01100000" &		-- fmt/type
									"0" &			-- reserved
									"000" &			-- tc
									"0000" &		-- reserved
									"0" &			-- no digest
									"0" &			-- not poisoned
									"00" &			-- attributes
									"00" &			-- reserved
									size(11 downto 2);	-- length
							tx_state <= header;
						end if;
					when header =>
						pcie_tx_valid <= '1';
						pcie_tx_sop <= '0';
						pcie_tx_eop <= '0';
						pcie_tx_data <=
								address(31 downto 2) &
								"00" &
								address(63 downto 32);
						tx_state <= data;
					when data =>
						if(data_valid = '1') then
							pcie_tx_valid <= '1';
							pcie_tx_sop <= '0';
							if(remaining <= 8) then
								pcie_tx_eop <= '1';
								tx_state <= idle;
							else
								pcie_tx_eop <= '0';
								tx_state <= unaffected;
							end if;
							remaining <= remaining - 8;
							pcie_tx_data <= data_data;
						end if;
				end case;
			end if;
		end if;
	end process;
end architecture;
