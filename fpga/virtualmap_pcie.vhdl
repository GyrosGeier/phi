library ieee;

use ieee.std_logic_1164.ALL;

entity virtualmap_pcie is
	port(
		-- asynchronous reset
		reset_n : in std_logic;

		-- data interface (read-only)
		address : in std_logic_vector(8 downto 0);
		rden : in std_logic;
		q : out std_logic_vector(63 downto 0);

		-- PCIe clock
		pcie_clk : in std_logic;

		-- receive side, internal PCIe interface (Avalon-ST)
		pcie_rx_ready : out std_logic;
		pcie_rx_valid : in std_logic;
		pcie_rx_data : in std_logic_vector(63 downto 0);
		pcie_rx_sop : in std_logic;
		pcie_rx_eop : in std_logic;
		pcie_rx_err : in std_logic;

		-- receive side, extra signals
		pcie_rx_mask : out std_logic;
		pcie_rx_bardec : in std_logic_vector(7 downto 0);

		-- send side, internal PCIe interface (Avalon-ST)
		pcie_tx_ready : in std_logic;
		pcie_tx_valid : out std_logic;
		pcie_tx_data : out std_logic_vector(63 downto 0);
		pcie_tx_sop : out std_logic;
		pcie_tx_eop : out std_logic;
		pcie_tx_err : out std_logic;

		-- arbiter interface
		pcie_tx_req : out std_logic;
		pcie_tx_start : in std_logic;

		-- power management
		pcie_cpl_pending : out std_logic;

		-- completer ID (from configuration space)
		pcie_cmpl_id : in std_logic_vector(15 downto 0)
	);
end entity;

architecture rtl of virtualmap_pcie is
	-- PCIe header is 2 QWORDs
	type rx_state_t is (header1, header2, data, ignore);
	signal rx_state : rx_state_t;

	type tx_state_t is (idle, waiting, header, data);
	signal tx_state : tx_state_t;

	signal cmpl_requester_id : std_logic_vector(15 downto 0);
	signal cmpl_tag : std_logic_vector(7 downto 0);
	signal cmpl_tc : std_logic_vector(2 downto 0);
	signal cmpl_data : std_logic_vector(63 downto 0);
	signal cmpl_strobe : std_logic;

	signal reg_address : std_logic_vector(8 downto 0);
	signal reg_data : std_logic_vector(63 downto 0);
	signal reg_rden : std_logic;
	signal reg_wren : std_logic;
	signal reg_q : std_logic_vector(63 downto 0);
begin
	virtualmap_inst : entity work.virtualmap
		port map(
			clock => pcie_clk,

			address_a => reg_address,
			data_a => reg_data,
			rden_a => reg_rden,
			wren_a => reg_wren,
			q_a => reg_q,

			address_b => address,
			data_b => (others => '0'),
			rden_b => rden,
			wren_b => '0',
			q_b => q
		);

	pcie_rx_ready <= '1';
	pcie_rx_mask <= '0';

	process(reset_n, pcie_clk) is
		variable pkt_fmt : std_logic_vector(1 downto 0);
		variable pkt_type : std_logic_vector(4 downto 0);
		variable pkt_tc : std_logic_vector(2 downto 0);
		variable pkt_td : std_logic;
		variable pkt_ep : std_logic;
		variable pkt_attr : std_logic_vector(1 downto 0);
		variable pkt_length : std_logic_vector(9 downto 0);
		variable pkt_requester_id : std_logic_vector(15 downto 0);
		variable pkt_tag : std_logic_vector(7 downto 0);
		variable pkt_last_be : std_logic_vector(3 downto 0);
		variable pkt_first_be : std_logic_vector(3 downto 0);
		variable pkt_address : std_logic_vector(63 downto 0);
	begin
		if(reset_n = '0') then
			rx_state <= header1;
			cmpl_strobe <= '0';
			reg_rden <= '0';
			reg_wren <= '0';
		elsif(rising_edge(pcie_clk)) then
			cmpl_strobe <= '0';
			reg_rden <= '0';
			reg_wren <= '0';
			if(pcie_rx_valid = '1') then
				if(pcie_rx_sop = '1') then
					-- state machine is reset by SOP
					pkt_requester_id := pcie_rx_data(63 downto 48);
					pkt_tag := pcie_rx_data(47 downto 40);
					pkt_last_be := pcie_rx_data(39 downto 36);
					pkt_first_be := pcie_rx_data(35 downto 32);
					pkt_fmt := pcie_rx_data(30 downto 29);
					pkt_type := pcie_rx_data(28 downto 24);
					pkt_tc := pcie_rx_data(22 downto 20);
					pkt_td := pcie_rx_data(15);
					pkt_ep := pcie_rx_data(14);
					pkt_attr := pcie_rx_data(13 downto 12);
					pkt_length := pcie_rx_data(9 downto 0);

					if(pcie_rx_bardec /= "00000010") then
						-- not BAR 0
						rx_state <= ignore;
					elsif(pkt_type /= "00000") then
						-- not memory read/write
						rx_state <= ignore;
					elsif(pkt_length /= "0000000010" or pkt_last_be /= "1111" or pkt_first_be /= "1111") then
						-- not a QWORD access
						-- TODO: generate error
						rx_state <= ignore;
					else
						rx_state <= header2;
					end if;
				else
					case rx_state is
						when header1 =>
							-- should not be here
							null;
						when header2 =>
							if(pkt_fmt(0) = '1') then
								-- 64 bit address
								pkt_address := pcie_rx_data(63 downto 2) & "00";
							else
								-- 32 bit address
								pkt_address := x"00000000" & pcie_rx_data(31 downto 2) & "00";
							end if;
							if(pkt_address(2 downto 0) /= "000") then
								-- not aligned
								-- TODO: generate error
								rx_state <= ignore;
							elsif(pkt_fmt(1) = '1') then
								-- data attached => write
								if(pcie_rx_eop = '0') then
									rx_state <= data;
								else
									rx_state <= header1;
								end if;
							else
								-- no data => read
								rx_state <= header1;
								cmpl_requester_id <= pkt_requester_id;
								cmpl_tag <= pkt_tag;
								cmpl_tc <= pkt_tc;
								-- TODO hack hack hack hack
								--cmpl_data <= reg_data;
								cmpl_strobe <= '1';
								reg_address <= pkt_address(11 downto 3);
								reg_rden <= '1';
							end if;
						when data =>
							reg_address <= pkt_address(11 downto 3);
							reg_data <= pcie_rx_data;
							reg_wren <= '1';
							rx_state <= header1;
						when ignore =>
							if(pcie_rx_eop = '1') then
								rx_state <= header1;
							end if;
					end case;
				end if;
			end if;
		end if;
	end process;

	-- TODO hack hack hack hack
	cmpl_data <= reg_q;

	pcie_tx_req <= '1' when tx_state = waiting else
		       '0';

	pcie_cpl_pending <= '1' when tx_state /= idle else
			    '0';

	process(reset_n, pcie_clk) is
		constant cmpl_fmt : std_logic_vector(1 downto 0) := "10";
		constant cmpl_type : std_logic_vector(4 downto 0) := "01010";
		-- tc is a signal
		constant cmpl_td : std_logic := '0';
		constant cmpl_ep : std_logic := '0';
		constant cmpl_attr : std_logic_vector(1 downto 0) := "00";
		constant cmpl_length : std_logic_vector(9 downto 0) := "0000000010";
		-- completer id is an input signal
		constant cmpl_status : std_logic_vector(2 downto 0) := "000";
		constant cmpl_bcm : std_logic := '0';
		constant cmpl_byte_count : std_logic_vector(11 downto 0) := "000000001000";
		-- requester id is a signal
		-- tag is a signal
		constant cmpl_lower_address : std_logic_vector(6 downto 0) := "0000000";
		-- data is a signal
	begin
		if(reset_n = '0') then
			tx_state <= idle;
			pcie_tx_valid <= '0';
			pcie_tx_sop <= '0';
			pcie_tx_eop <= '0';
			pcie_tx_err <= '0';
		elsif(rising_edge(pcie_clk) and pcie_tx_ready = '0') then
			pcie_tx_valid <= '0';
			pcie_tx_sop <= '0';
			pcie_tx_eop <= '0';
			pcie_tx_err <= '0';
		elsif(rising_edge(pcie_clk) and pcie_tx_ready = '1') then
			pcie_tx_valid <= '0';
			pcie_tx_sop <= '0';
			pcie_tx_eop <= '0';
			pcie_tx_err <= '0';
			case tx_state is
				when idle =>
					if(cmpl_strobe = '1') then
						tx_state <= waiting;
					end if;
				when waiting =>
					if(pcie_tx_start = '1') then
						pcie_tx_valid <= '1';
						pcie_tx_data <= pcie_cmpl_id &
								cmpl_status &
								cmpl_bcm &
								cmpl_byte_count &
								"0" &
								cmpl_fmt &
								cmpl_type &
								"0" &
								cmpl_tc &
								"0000" &
								cmpl_td &
								cmpl_ep &
								cmpl_attr &
								"00" &
								cmpl_length;
						pcie_tx_sop <= '1';
						tx_state <= header;
					end if;
				when header =>
					pcie_tx_valid <= '1';
					pcie_tx_data <= x"00000000" &
							cmpl_requester_id &
							cmpl_tag &
							"0" &
							cmpl_lower_address;
					tx_state <= data;
				when data =>
					pcie_tx_valid <= '1';
					pcie_tx_data <= cmpl_data;
					pcie_tx_eop <= '1';
					tx_state <= idle;
			end case;
		end if;
	end process;
end architecture;
