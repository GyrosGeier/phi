library ieee;

use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;		-- TODO remove

entity phi is
	port(
		-- PCIe refclk (100 MHz HCSL)
		refclk : in std_logic;

		pcie_nperst : in std_logic;

		-- secondary refclk (125 MHz LVDS)
		fixedclk_serdes : in std_logic;

		pcie_rx : in std_logic_vector(3 downto 0);
		pcie_tx : out std_logic_vector(3 downto 0);

		refclk_select : out std_logic;
		vco_select : out std_logic;

		adc_a_oe_n, adc_b_oe_n : out std_logic;
		adc_a_data, adc_b_data : in std_logic_vector(11 downto 0);
		adc_a_data_ready, adc_b_data_ready : in std_logic;
		adc_a_out_of_range, adc_b_out_of_range : in std_logic;
		adc_a_powerdown, adc_b_powerdown : out std_logic;

		adc_a_cs, adc_b_cs : out std_logic;
		adc_a_sclk, adc_b_sclk : out std_logic;
		adc_a_sdio, adc_b_sdio : inout std_logic;

		adc_vga_sdo : in std_logic;
		adc_vga_clk : out std_logic;
		adc_vga_cs : out std_logic;
		adc_vga_sdi : out std_logic;

		rx_hi_vga_pwm : out std_logic;
		tx_hi_vga_pwm : out std_logic;

		rx_lo_vga_pwm : out std_logic;
		rx_lo_vga_enable : out std_logic;
		rx_lo_vga_hilo : out std_logic;

		rx_rf_switch : out std_logic;
		tx_rf_switch : out std_logic;

		synth_rx_lock_detect : in std_logic;
		synth_rx_MUXOUT : in std_logic;
		synth_rx_ce : out std_logic;		-- polarity?
		synth_rx_clk : out std_logic;
		synth_rx_data : out std_logic;
		synth_rx_latch_enable : out std_logic;

		synth_tx_lock_detect : in std_logic;
		synth_tx_MUXOUT : in std_logic;
		synth_tx_ce : out std_logic;		-- polarity?
		synth_tx_clk : out std_logic;
		synth_tx_data : out std_logic;
		synth_tx_latch_enable : out std_logic;

		dac_data_clk : in std_logic;
		dac_a_data, dac_b_data : out std_logic_vector(15 downto 0);

		dac_cs : out std_logic;
		dac_sdo : out std_logic;
		dac_sclk : out std_logic;
		dac_reset : out std_logic;
		dac_sdio : out std_logic;

		dac_iqsel : out std_logic;

		-- can be used as inputs
		gpio : out std_logic_vector(9 downto 0)
	);
end entity;

architecture rtl of phi is
	-- asynchronous resets
	signal pcie_npor : std_logic;		-- Power On Reset, active low

	-- clocks
	signal pcie_clk : std_logic;		-- PCIe application clock

	-- synchronous resets
	signal pcie_srst : std_logic;		-- everything but config space
	signal pcie_crst : std_logic;		-- configuration reset
	signal pcie_app_rstn : std_logic;	-- application reset

	-- receive side, internal PCIe interface (Avalon-ST)
	signal pcie_rx_ready : std_logic;
	signal pcie_rx_valid : std_logic;
	signal pcie_rx_data : std_logic_vector(63 downto 0);
	signal pcie_rx_sop : std_logic;
	signal pcie_rx_eop : std_logic;
	signal pcie_rx_err : std_logic;

	-- receive side, extra signals
	signal pcie_rx_mask : std_logic;
	signal pcie_rx_bardec : std_logic_vector(7 downto 0);

	-- send side, internal PCIe interface (Avalon-ST)
	signal pcie_tx_ready : std_logic;
	signal pcie_tx_valid : std_logic;
	signal pcie_tx_data : std_logic_vector(63 downto 0);
	signal pcie_tx_sop : std_logic;
	signal pcie_tx_eop : std_logic;
	signal pcie_tx_err : std_logic;

	-- PCIe transmit arbiter
	signal pcie_tx_req_start_top : std_logic;

	-- interrupts (legacy)
	signal app_int_sts : std_logic;
	signal app_int_ack : std_logic;

	-- interrupts (MSI)
	signal app_msi_req : std_logic;
	signal app_msi_num : std_logic_vector(4 downto 0);
	signal app_msi_tc : std_logic_vector(2 downto 0);
	signal app_msi_ack : std_logic;

	signal tl_cfg_add : std_logic_vector(3 downto 0);
	signal tl_cfg_ctl : std_logic_vector(31 downto 0);
	signal tl_cfg_ctl_wr : std_logic;
	signal tl_cfg_sts : std_logic_vector(52 downto 0);
	signal tl_cfg_sts_wr : std_logic;

	-- refuse powersave, as we have outstanding requests
	signal cpl_pending : std_logic;

	signal cfg_busdev : std_logic_vector(12 downto 0);

	-- link layer state notifications
	signal l2_exit : std_logic;
	signal hotrst_exit : std_logic;
	signal dlup_exit : std_logic;

	signal ltssm : std_logic_vector(4 downto 0);
	signal suc_spd_neg : std_logic;
	signal powerdown_ext : std_logic_vector(1 downto 0);
	signal reset_status : std_logic;

	signal pll_powerdown : std_logic;
	signal gxb_powerdown : std_logic;

	-- PLL status
	signal pll_locked : std_logic;

	-- analog calibration
	signal cal_blk_clk : std_logic;		-- 100 MHz

	-- GXB reconfig (magic)
	signal pcie_reconfig_clk : std_logic;
	signal pcie_reconfig_fromgxb : std_logic_vector(4 downto 0);
	signal pcie_reconfig_togxb : std_logic_vector(3 downto 0);
	signal pcie_reconfig_busy : std_logic;

	-- debug port pin mapping
	alias debug_clk : std_logic is gpio(3);
	alias debug0 : std_logic is gpio(4);
	alias debug1 : std_logic is gpio(1);
	alias debug2 : std_logic is gpio(2);
	alias debug3 : std_logic is gpio(9);
	alias debug4 : std_logic is gpio(0);
	alias debug5 : std_logic is gpio(7);
	alias debug6 : std_logic is gpio(8);
	alias debug7 : std_logic is gpio(5);
	alias debug_gnd : std_logic is gpio(6);

	signal debug_valid : std_logic;
	signal debug_data : std_logic_vector(7 downto 0);

	signal counter : unsigned(7 downto 0);
begin
	refclk_select <= '1';
	vco_select <= '1';

	debug_inst : entity work.gpio
		port map(
			-- gate clock with valid bit
			datain_h => "0" & debug_data & debug_valid,
			datain_l => "0" & debug_data & "0",
			outclock => refclk,
			dataout(0) => debug_clk,
			dataout(1) => debug0,
			dataout(2) => debug1,
			dataout(3) => debug2,
			dataout(4) => debug3,
			dataout(5) => debug4,
			dataout(6) => debug5,
			dataout(7) => debug6,
			dataout(8) => debug7,
			dataout(9) => debug_gnd
		);

	counter <= (others => '0') when pcie_nperst = '0' else
		   counter + 1 when rising_edge(refclk);

	debug_valid <= '1';
	debug_data <= std_logic_vector(counter);

	pll_powerdown <= '0';
	gxb_powerdown <= '0';

	pll_inst : entity work.pll
		port map(
			inclk0 => fixedclk_serdes,
			c0 => pcie_reconfig_clk,
			c1 => cal_blk_clk,
			locked => pll_locked
		);

	pcie_npor <= pcie_nperst and pll_locked;

	-- interrupts (legacy)
	app_int_sts <= '0';
	--app_int_ack

	-- interrupts (MSI)
	app_msi_req <= '0';
	app_msi_num <= (others => '0');
	app_msi_tc <= (others => '0');
	--app_msi_ack

	phi_pcie_app_inst : entity work.phi_pcie_app
		port map(
			-- asynchronous reset
			reset_n => pcie_app_rstn,	-- FIXME: this is synchronous

			-- PCIe clock
			pcie_clk => pcie_clk,

			-- receive side, internal PCIe interface (Avalon-ST)
			pcie_rx_ready => pcie_rx_ready,
			pcie_rx_valid => pcie_rx_valid,
			pcie_rx_data => pcie_rx_data,
			pcie_rx_sop => pcie_rx_sop,
			pcie_rx_eop => pcie_rx_eop,
			pcie_rx_err => pcie_rx_err,

			-- receive side, extra signals
			pcie_rx_mask => pcie_rx_mask,
			pcie_rx_bardec => pcie_rx_bardec,

			-- send side, internal PCIe interface (Avalon-ST)
			pcie_tx_ready => pcie_tx_ready,
			pcie_tx_valid => pcie_tx_valid,
			pcie_tx_data => pcie_tx_data,
			pcie_tx_sop => pcie_tx_sop,
			pcie_tx_eop => pcie_tx_eop,
			pcie_tx_err => pcie_tx_err,

			-- PCIe transmit arbiter interface
			-- this is the top of the arbiter tree, so
			-- req and start are connected here
			pcie_tx_req => pcie_tx_req_start_top,
			pcie_tx_start => pcie_tx_req_start_top,

			-- PCIe power management
			pcie_cpl_pending => cpl_pending,

			-- completer ID (from configuration space)
			pcie_cmpl_id => cfg_busdev & "000"
		);

	pcie_inst : entity work.pcie
		port map(
			-- reset signals
			npor => pcie_npor,
			crst => pcie_crst,
			srst => pcie_srst,
			pll_powerdown => pll_powerdown,
			gxb_powerdown => gxb_powerdown,

			-- clocks
			cal_blk_clk => cal_blk_clk,		-- 50..125 MHz
			fixedclk_serdes => fixedclk_serdes,		-- 125 MHz, direct
			core_clk_out => pcie_clk,		-- clock to PCIe application domain
			pld_clk => pcie_clk,			-- feed back into block

			-- external PCIe interface (serial)
			refclk => refclk,
			rx_in0 => pcie_rx(0),
			rx_in1 => pcie_rx(1),
			rx_in2 => pcie_rx(2),
			rx_in3 => pcie_rx(3),
			tx_out0 => pcie_tx(0),
			tx_out1 => pcie_tx(1),
			tx_out2 => pcie_tx(2),
			tx_out3 => pcie_tx(3),

			-- internal PCIe interface (Avalon-ST)
			rx_st_ready0 => pcie_rx_ready,
			rx_st_valid0 => pcie_rx_valid,
			rx_st_data0 => pcie_rx_data,
			rx_st_sop0 => pcie_rx_sop,
			rx_st_eop0 => pcie_rx_eop,
			rx_st_err0 => pcie_rx_err,

			rx_st_mask0 => pcie_rx_mask,
			rx_st_be0 => open,			-- deprecated, don't use
			rx_st_bardec0 => pcie_rx_bardec,

			tx_st_ready0 => pcie_tx_ready,
			tx_st_valid0 => pcie_tx_valid,
			tx_st_data0 => pcie_tx_data,
			tx_st_sop0 => pcie_tx_sop,
			tx_st_eop0 => pcie_tx_eop,
			tx_st_err0 => pcie_tx_err,

			cpl_pending => cpl_pending,
			cpl_err => (others => '0'),		-- not implemented

			pex_msi_num => (others => '0'),		-- not implemented

			dlup_exit => dlup_exit,
			hotrst_exit => hotrst_exit,
			l2_exit => l2_exit,

			ltssm => ltssm,
			suc_spd_neg => suc_spd_neg,
			powerdown_ext => powerdown_ext,
			reset_status => reset_status,

			-- interrupts (legacy)
			app_int_sts => app_int_sts,
			app_int_ack => app_int_ack,

			-- interrupts (MSI)
			app_msi_req => app_msi_req,
			app_msi_num => app_msi_num,
			app_msi_tc => app_msi_tc,
			app_msi_ack => app_msi_ack,

			-- configuration space readout
			tl_cfg_add => tl_cfg_add,
			tl_cfg_ctl => tl_cfg_ctl,
			tl_cfg_ctl_wr => tl_cfg_ctl_wr,
			tl_cfg_sts => tl_cfg_sts,
			tl_cfg_sts_wr => tl_cfg_sts_wr,

			-- power management (unused so far)
			pm_auxpwr => '0',			-- L2 state not supported anyway
			pm_data => "0011001001",		-- 5W
			pm_event => '0',			-- do not generate PME
			pme_to_cr => '0',			-- do not acknowledge PME
			pme_to_sr => open,			-- ignore PME (FIXME)

			-- local management interface (unused so far)
			lmi_addr => (others => '0'),
			lmi_din => (others => '0'),
			lmi_rden => '0',
			lmi_wren => '0',
			lmi_ack => open,
			lmi_dout => open,

			-- GXB reconfig (magic)
			reconfig_clk => pcie_reconfig_clk,
			reconfig_fromgxb => pcie_reconfig_fromgxb,
			reconfig_togxb => pcie_reconfig_togxb,
			busy_altgxb_reconfig => pcie_reconfig_busy,

			-- simulation only things (tie to zero)
			hpg_ctrler => (others => '0'),
			pclk_in => '0',
			phystatus_ext => '0',
			pipe_mode => '0',
			rxdata0_ext => (others => '0'),
			rxdata1_ext => (others => '0'),
			rxdata2_ext => (others => '0'),
			rxdata3_ext => (others => '0'),
			rxdatak0_ext => '0',
			rxdatak1_ext => '0',
			rxdatak2_ext => '0',
			rxdatak3_ext => '0',
			rxelecidle0_ext => '0',
			rxelecidle1_ext => '0',
			rxelecidle2_ext => '0',
			rxelecidle3_ext => '0',
			rxstatus0_ext => (others => '0'),
			rxstatus1_ext => (others => '0'),
			rxstatus2_ext => (others => '0'),
			rxstatus3_ext => (others => '0'),
			rxvalid0_ext => '0',
			rxvalid1_ext => '0',
			rxvalid2_ext => '0',
			rxvalid3_ext => '0',
			test_in => (others => '0')
		);

	pcie_reconfig_inst : entity work.pcie_reconfig
		port map(
			-- GXB reconfig (magic)
			reconfig_clk => pcie_reconfig_clk,
			reconfig_fromgxb => pcie_reconfig_fromgxb,
			reconfig_togxb => pcie_reconfig_togxb,
			busy => pcie_reconfig_busy
		);

	pcie_rs_hip_inst : entity work.pcie_rs_hip
		port map(
			dlup_exit => dlup_exit,
			hotrst_exit => hotrst_exit,
			l2_exit => l2_exit,
			ltssm => ltssm,
			npor => pcie_npor,
			pld_clk => pcie_clk,
			test_sim => '0',

			app_rstn => pcie_app_rstn,
			crst => pcie_crst,
			srst => pcie_srst
		);

	altpcierd_tl_cfg_sample_inst : entity work.altpcierd_tl_cfg_sample
		port map(
			rstn => pcie_srst,
			pld_clk => pcie_clk,
			tl_cfg_add => tl_cfg_add,
			tl_cfg_ctl => tl_cfg_ctl,
			tl_cfg_ctl_wr => tl_cfg_ctl_wr,
			tl_cfg_sts => tl_cfg_sts,
			tl_cfg_sts_wr => tl_cfg_sts_wr,

			cfg_busdev => cfg_busdev,
			cfg_devcsr => open,
			cfg_linkcsr => open,
			cfg_prmcsr => open,

			cfg_io_bas => open,
			cfg_io_lim => open,
			cfg_np_bas => open,
			cfg_np_lim => open,
			cfg_pr_bas => open,
			cfg_pr_lim => open,

			cfg_tcvcmap => open,

			cfg_msicsr => open
		);
end architecture;
