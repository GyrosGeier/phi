#include <linux/module.h>
#include <linux/pci.h>

#define PHI_NUM_DMA_PAGES 512

struct phi_context
{
	// PCI device
	struct pci_dev *pdev;

	// mapped registers
	u64 volatile *reg;

	// virtual memory mapping
	u64 volatile *virtualmap;

	// DMA pages
	struct page *dma_pages[PHI_NUM_DMA_PAGES];

	// DMA handles
	dma_addr_t dma_addresses[PHI_NUM_DMA_PAGES];
};

static void phi_free_dma_pages(
		struct phi_context *const ctx,
		unsigned int count)
{
	unsigned int i;

	for(i = 0; i < count; ++i)
		__free_pages(ctx->dma_pages[i], HPAGE_PMD_ORDER);
}

static void phi_unmap_dma_pages(
		struct phi_context *const ctx,
		unsigned int count)
{
	struct pci_dev *const pdev = ctx->pdev;
	struct device *const dev = &pdev->dev;

	unsigned int i;

	for(i = 0; i < count; ++i)
		dma_unmap_page(
				dev,
				ctx->dma_addresses[i],
				HPAGE_PMD_SIZE,
				DMA_FROM_DEVICE);
}

static int phi_probe(
		struct pci_dev *const pdev,
		struct pci_device_id const *const id)
{
	struct device *const dev = &pdev->dev;

	unsigned int i;

	int err;

	struct phi_context *ctx;

	err = pcim_enable_device(pdev);
	if(err < 0)
		return err;

	ctx = devm_kzalloc(dev, sizeof *ctx, GFP_KERNEL);
	if(!ctx)
		return -ENOMEM;

	dev->driver_data = ctx;
	ctx->pdev = pdev;

	err = pci_set_dma_mask(pdev, DMA_BIT_MASK(64));
	if(err < 0)
		return err;

	err = pci_set_consistent_dma_mask(pdev, DMA_BIT_MASK(64));
	if(err < 0)
		return err;

	pci_set_master(pdev);

	ctx->virtualmap = pcim_iomap(pdev, 1, 4096);
	if(!ctx->virtualmap)
		return -ENOMEM;

	for(i = 0; i < PHI_NUM_DMA_PAGES; ++i)
	{
		// we use 2MB hugepages.
		ctx->dma_pages[i] = alloc_pages(0, HPAGE_PMD_ORDER);
		if(!ctx->dma_pages[i])
		{
			printk(KERN_INFO "Failed to allocate DMA buffer %u\n", i);
			phi_free_dma_pages(ctx, i);
			return -ENOMEM;
		}
	}

	for(i = 0; i < PHI_NUM_DMA_PAGES; ++i)
	{
		ctx->dma_addresses[i] = dma_map_page_attrs(dev, ctx->dma_pages[i], 0, HPAGE_PMD_SIZE, DMA_FROM_DEVICE, 0);
		if(dma_mapping_error(dev, ctx->dma_addresses[i]))
		{
			printk(KERN_INFO "DMA mapping error for DMA buffer %u\n", i);
			phi_unmap_dma_pages(ctx, i);
			phi_free_dma_pages(ctx, PHI_NUM_DMA_PAGES);
			return -ENOMEM;
		}
		ctx->virtualmap[i] = ctx->dma_addresses[i];
	}

	for(i = 0; i < PHI_NUM_DMA_PAGES; ++i)
	{
		u64 const readback = ctx->virtualmap[i];
		if(readback != ctx->dma_addresses[i])
		{
			printk(KERN_INFO "Readback failed for DMA buffer %u: %llx != %llx\n", i, readback, ctx->dma_addresses[i]);
			return -EIO;
		}
	}

	return 0;
}

static void phi_remove(
		struct pci_dev *const pdev)
{
	struct device *const dev = &pdev->dev;
	struct phi_context *const ctx = dev->driver_data;

	phi_unmap_dma_pages(ctx, PHI_NUM_DMA_PAGES);
	phi_free_dma_pages(ctx, PHI_NUM_DMA_PAGES);

	pci_disable_device(pdev);
}

static struct pci_device_id const phi_ids[] =
{
	{ PCI_DEVICE(0x1172, 0xcafe) },
	{ 0, }
};

static struct pci_driver phi_driver =
{
	.name = "phi",
	.id_table = phi_ids,
	.probe = phi_probe,
	.remove = phi_remove,
};

static int __init phi_init(void)
{
	return pci_register_driver(&phi_driver);
}

static void __exit phi_exit(void)
{
	pci_unregister_driver(&phi_driver);
}

module_init(phi_init);
module_exit(phi_exit);

MODULE_AUTHOR("Simon Richter <Simon.Richter@hogyros.de>");
MODULE_DESCRIPTION("Per Vices Phi (alternate firmware)");
MODULE_LICENSE("GPL");
MODULE_DEVICE_TABLE(pci, phi_ids);
